from django.urls import path
from .views import all_transaction_view

app_name = 'transaksi'

urlpatterns = [
    path('', all_transaction_view, name='all-transactions')
]
