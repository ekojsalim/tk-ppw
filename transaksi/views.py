from django.shortcuts import render
from .models import Transaksi


def all_transaction_view(request):
    transaksi_all = Transaksi.objects.all().order_by("-tanggal")
    context = {
        "transaksi_all": transaksi_all
    }
    return render(request, "transaksi/semua.html", context)
