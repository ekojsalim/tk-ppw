from django import forms
from .models import Transaksi
from kupon.models import Kupon


class BeliBarangForm(forms.Form):
    def __init__(self, stok, *args, **kwargs):
        super(BeliBarangForm, self).__init__(*args, **kwargs)
        self.fields['jumlah'] = forms.IntegerField(min_value=1,
                                                   max_value=stok)
    nama_pembeli = forms.CharField(max_length=100, label="Nama Pembeli")
    kupon = forms.ModelChoiceField(
        queryset=Kupon.objects.all(), required=False)
    jumlah = forms.IntegerField(min_value=1)
