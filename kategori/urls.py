from django.urls import path
from .views import kategori_view

app_name = 'kategori'

urlpatterns = [
    path('<str:kategori>', kategori_view, name='kategori'),
    path('', kategori_view, name='kategori-default')
]
