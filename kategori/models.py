from django.db import models


class Kategori(models.Model):
    nama = models.CharField(max_length=70, primary_key=True)
