from django.urls import path
from .views import landing

urlpatterns = [
    path('<str:kategori>', landing, name='landing-kategori'),
    path('', landing, name='landing')
]
