from django.urls import path
from .views import admin_view, add_view

app_name = 'shopadmin'

urlpatterns = [
    path('', admin_view, name='shop-admin'),
    path('add/<str:name>', add_view, name='add')
]
