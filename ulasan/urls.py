from django.urls import path
from .views import semua_ulasan_view

app_name = 'ulasan'

urlpatterns = [
    path('', semua_ulasan_view, name='all-ulasan')
]
