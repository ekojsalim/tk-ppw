from django import forms
from .models import Ulasan


class UlasanForm(forms.ModelForm):
    bintang = forms.ChoiceField(
        choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')])

    def clean_bintang(self):
        bintang = int(self.cleaned_data['bintang'])
        if bintang < 0 or bintang > 5:
            raise forms.ValidationError("Rating out of bounds!")
        return bintang

    class Meta:
        model = Ulasan
        fields = '__all__'
        widgets = {
            'barang': forms.HiddenInput()
        }
