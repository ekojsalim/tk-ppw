from django.shortcuts import render
from .models import Ulasan


def semua_ulasan_view(request):
    ulasan_all = Ulasan.objects.all().order_by("-tanggal")
    context = {
        "ulasan_all": ulasan_all
    }
    return render(request, "ulasan/semua.html", context)
