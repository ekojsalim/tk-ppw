from django.db import models
from barang.models import Barang


class Ulasan(models.Model):
    nama_pengulas = models.CharField(max_length=100)
    bintang = models.PositiveSmallIntegerField()
    barang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    pesan = models.TextField()
    tanggal = models.DateTimeField(auto_now_add=True)
