from django.db import models


class Kupon(models.Model):
    kode = models.CharField(max_length=30)
    persen_diskon = models.DecimalField(decimal_places=2, max_digits=5)
    expired = models.DateTimeField()
    min_harga = models.DecimalField(decimal_places=2, max_digits=30)

    def __str__(self):
        return self.kode
