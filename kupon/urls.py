from django.urls import path
from .views import semua_kupon_view

app_name = 'kupon'

urlpatterns = [
    path('', semua_kupon_view, name='semua-kupon')
]
