from django.shortcuts import render
from .models import Kupon


def semua_kupon_view(request):
    min_discount = request.GET.get("min-discount", 0)
    min_discount = min_discount if min_discount else 0
    coupon_filter = request.GET.get("coupon", "")
    kupon_all = Kupon.objects.filter(
        kode__contains=coupon_filter, persen_diskon__gte=min_discount)
    context = {
        "kupon_all": kupon_all,
        "min_discount": min_discount,
        "coupon_filter": coupon_filter
    }
    return render(request, "kupon/semua.html", context)
